import {
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  RecoverEvent,
  RemoveEvent,
  SoftRemoveEvent,
  UpdateEvent,
} from 'typeorm';
import { Logger } from '@nestjs/common';

@EventSubscriber()
export class EntitySubscriber implements EntitySubscriberInterface {
  private readonly _logger = new Logger(EntitySubscriber.name);

  /**
   * Called after entity insertion.
   */
  afterInsert(event: InsertEvent<any>) {
    this._logger.log(
      `Created ${event.metadata.tableName} with id ${event?.entity?.id} and public identifier ${event?.entity?.identifier}`,
    );
  }

  afterUpdate(event: UpdateEvent<any>) {
    this._logger.log(
      `Updated ${event?.metadata?.tableName} with id ${event?.entity?.id} and public identifier ${event?.entity?.identifier}`,
    );
  }

  /**
   * Called after entity removal.
   */
  afterRemove(event: RemoveEvent<any>) {
    if (event.entityId) {
      this._logger.log(
        `Removed ${event.metadata.tableName} with id ${event.entity.id} and public identifier ${event.entity.identifier}`,
      );
    } else {
      this._logger.verbose(`Removed ${event.metadata.tableName}`);
    }
  }

  /**
   * Called after entity removal.
   */
  afterSoftRemove(event: SoftRemoveEvent<any>) {
    this._logger.log(`Marked as removed from ${event?.metadata?.tableName}`);
  }

  /**
   * Called after entity removal.
   */
  afterRecover(event: RecoverEvent<any>) {
    this._logger.log(
      `Recovered ${event.metadata.tableName} with id ${event.entity.id} and public identifier ${event.entity.identifier}`,
    );
  }
}
