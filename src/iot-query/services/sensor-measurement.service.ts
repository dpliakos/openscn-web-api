import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SensorMeasurementEntity } from '../entities/sensor-measurement.entity';
import { EntityService } from '../../common';

@Injectable()
export class SensorMeasurementService extends EntityService<SensorMeasurementEntity> {
  private readonly _logger = new Logger(
    `IoTQuery/${SensorMeasurementService.name}`,
  );

  constructor(
    @InjectRepository(SensorMeasurementEntity)
    private readonly _sensorMeasurementEntityRepository: Repository<SensorMeasurementEntity>,
  ) {
    super(SensorMeasurementEntity.name);
    this.repository = _sensorMeasurementEntityRepository;
  }
}
