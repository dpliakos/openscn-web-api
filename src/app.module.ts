import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CommonModule } from './common';
import configuration from './config/configuration';
import { DatabaseModule } from './database/database.module';
import { AppController } from './app.controller';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { IotDomainModule } from './iot-domain/iot-domain.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { IoTIngressModule } from './iot-ingress/iot-ingress.module';
import { IotQueryModule } from './iot-query/iot-query.module';
import { IotTasksModule } from './iot-tasks/iot-tasks.module';
import { BullModule } from '@nestjs/bullmq';
import { IConfiguration } from './config/config.inteface';
import { ClientModule } from './client/client.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      isGlobal: true,
    }),
    EventEmitterModule.forRoot({
      global: true,
      wildcard: false,
      delimiter: '.',
      newListener: true,
      removeListener: true,
      verboseMemoryLeak: true,
      ignoreErrors: false,
    }),
    DatabaseModule,
    CommonModule,
    AuthModule,
    UsersModule,
    IotDomainModule,
    IoTIngressModule,
    IotQueryModule,
    BullModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        const config = configService.get<IConfiguration['redis']>('redis');
        return {
          connection: {
            host: config.url,
            port: config.port,
            username: config.username,
            password: config.password,
          },
        };
      },
      inject: [ConfigService],
    }),
    IotTasksModule,
    ClientModule,
    // ServeStaticModule.forRoot({
    //   rootPath: path.resolve('static'),
    // }),
  ],
  controllers: [AppController],
})
export class AppModule {}
