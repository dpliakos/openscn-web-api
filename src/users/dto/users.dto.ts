import {
  IsEmail,
  IsNotEmpty,
  IsAlphanumeric,
  IsString,
  IsOptional,
  MinLength,
  MaxLength,
} from 'class-validator';

/**
 *
 * Data transfer objects for user
 *
 */

export class CreateUserDTO {
  @IsNotEmpty()
  @IsAlphanumeric()
  @MinLength(3)
  @MaxLength(25)
  username: string;

  @IsNotEmpty()
  @IsEmail()
  @MaxLength(255)
  email: string;

  @IsNotEmpty()
  @MinLength(6)
  @MaxLength(32)
  password: string;

  @IsNotEmpty()
  @MinLength(6)
  @MaxLength(32)
  passwordRepeat: string;

  @IsString()
  @IsOptional()
  @MinLength(1)
  @MaxLength(30)
  firstName: string;

  @IsString()
  @IsOptional()
  @MinLength(1)
  @MaxLength(30)
  lastName: string;

  @IsString()
  @IsOptional()
  @MaxLength(256)
  description: string;
}

export class UpdateUserDTO {
  @IsOptional()
  @IsNotEmpty()
  @IsAlphanumeric()
  @MinLength(3)
  @MaxLength(25)
  username: string;

  @IsString()
  @IsOptional()
  @MinLength(1)
  @MaxLength(30)
  firstName: string;

  @IsString()
  @IsOptional()
  @MinLength(1)
  @MaxLength(30)
  lastName: string;

  @IsString()
  @IsOptional()
  @MaxLength(256)
  description: string;
}

export class FindOneParams {
  @IsAlphanumeric()
  username: string;
}
