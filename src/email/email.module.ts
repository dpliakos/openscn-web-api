import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';
import * as path from 'path';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';
import { EmailService } from './services/email.service';
import { ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MailerModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        return {
          transport: {
            host: configService.get('email').host,
            port: configService.get('email').port,
            ignoreTLS: configService.get('email').ignoreTLS === true,
            secure: configService.get('email').secure === true,
          },
          defaults: {
            from: configService.get('email').defaultSender,
          },
          preview: false,
          template: {
            dir: path.resolve(__dirname, 'templates'),
            adapter: new PugAdapter({
              inlineCssEnabled: false,
            }),
            options: {
              strict: true,
              partials: {
                dir: path.join(__dirname, 'templates/partials'),
                options: {
                  strict: true,
                },
              },
            },
          },
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [EmailService],
  exports: [EmailService],
})
export class EmailModule {}
