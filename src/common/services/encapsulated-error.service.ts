import {
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { EncapsulatedError } from '../errors';

@Injectable()
export class EncapsulatedErrorService {
  private readonly _logger = new Logger(EncapsulatedError.name);

  log(logger: Logger, err: Error) {
    if (logger) {
      logger.error(err.stack ? err.stack : err);
    } else {
      this._logger.error(err.stack ? err.stack : err);
    }
  }

  formatError(logger: Logger, err: Error) {
    if (err instanceof EncapsulatedError && err.presentableError) {
      if ((err as EncapsulatedError).error) {
        this.log(logger, err.error);
      } else {
        this.log(logger, err.presentableError);
      }

      return err.presentableError;
    } else {
      this.log(logger, err);
      return new InternalServerErrorException();
    }
  }

  handleControllerError(logger: Logger, err: Error) {
    throw this.formatError(logger, err);
  }
}
