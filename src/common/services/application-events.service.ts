import { Injectable, Logger } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { IApplicationEvent } from '../interfaces';
import { InjectRepository } from '@nestjs/typeorm';
import { ApplicationEventEntity } from '../entities/application-event.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ApplicationEventsService {
  private readonly _logger = new Logger(ApplicationEventsService.name);

  constructor(
    private readonly _eventEmitter: EventEmitter2,
    @InjectRepository(ApplicationEventEntity)
    private readonly _applicationEventRepository: Repository<ApplicationEventEntity>,
  ) {}

  static formatReceiverLog(
    eventSubject: IApplicationEvent['subject'],
    event?: IApplicationEvent['payload'],
  ) {
    return `[Event/received] ${eventSubject} ${
      (event as any)?.eventIdentifier
        ? ' - ' + (event as any).eventIdentifier
        : ''
    }`;
  }

  emit(event: IApplicationEvent) {
    const payload = event.payload ? JSON.stringify(event.payload) : undefined;

    this._applicationEventRepository
      .save({
        subject: event.subject,
        payload,
      })
      .then((result) => {
        const formattedPayload = {
          ...event.payload,
          eventIdentifier: result.eventIdentifier,
        };
        this._logger.verbose(
          `[Event/published] ${result.subject} - ${result.eventIdentifier}`,
        );
        setImmediate(() =>
          this._eventEmitter.emit(event.subject, formattedPayload),
        );
      });
  }
}
