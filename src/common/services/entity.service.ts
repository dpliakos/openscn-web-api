import { EntityBase } from '../entities/entity-base';
import {
  DeepPartial,
  FindManyOptions,
  FindOneOptions,
  FindOptionsWhere,
  Repository,
} from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { NotFoundException } from '@nestjs/common';

/**
 *
 * @abstract
 * @class EntityService
 *
 * An Entity service. It is used to implement the boilerplate CRUD operations of an entity
 *
 */
export abstract class EntityService<T extends EntityBase> {
  protected constructor(private readonly _name?: string) {}

  protected repository: Repository<T>;

  get name(): string {
    return this._name;
  }

  get manager() {
    return this.repository.manager;
  }

  async findAll(options?: FindManyOptions<T>): Promise<T[]> {
    return this.repository.find(options);
  }

  async findOne(options?: FindOneOptions<T>): Promise<T> {
    return this.repository.findOne(options);
  }

  async findAndCount(options: FindManyOptions<T>): Promise<[T[], number]> {
    return this.repository.findAndCount(options);
  }

  async count(options: FindManyOptions<T>): Promise<number> {
    return this.repository.count(options);
  }

  async remove(id: number) {
    return this.repository.softDelete(id);
  }

  async softRemove(id: number | FindOptionsWhere<T>) {
    return this.repository.softDelete(id);
  }

  async create(item: DeepPartial<T>) {
    return this.repository.create(item);
  }

  async save(item: DeepPartial<T>) {
    return this.repository.save(item);
  }

  async update(id: number, fields: QueryDeepPartialEntity<T>) {
    return this.repository.update(id, fields);
  }

  async updateMany(
    options: FindOptionsWhere<T>,
    fields: QueryDeepPartialEntity<T>,
  ) {
    return this.repository.update(options, fields);
  }

  queryBuilder(alias?: string) {
    return this.repository.createQueryBuilder(alias);
  }

  async findOneOrThrow(
    options?: FindOneOptions<T>,
    identifier?: string,
  ): Promise<T> {
    const result = await this.findOne(options);
    if (!result) {
      throw new NotFoundException(
        `${this.name} ${identifier ?? ''} was not found`,
      );
    }
    return result;
  }

  formatFindOptionsWithSearch(
    findOptions: FindOptionsWhere<T>,
    searchFactors: Record<string, unknown>[],
  ): FindOptionsWhere<T>[] {
    const alternatives = [];

    if (searchFactors?.length > 0) {
      for (let i = 0; i < searchFactors.length; i++) {
        const alternative = {
          ...findOptions,
          ...searchFactors[i],
        };
        alternatives.push(alternative);
      }
    } else {
      alternatives.push(findOptions);
    }

    return alternatives;
  }
}
