import { Injectable } from '@nestjs/common';
import { EntityService } from './entity.service';
import { Person } from '../entities';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PersonService extends EntityService<Person> {
  constructor(
    @InjectRepository(Person)
    private readonly _personRepository: Repository<Person>,
  ) {
    super();
    this.repository = this._personRepository;
  }
}
