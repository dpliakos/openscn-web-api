import { Column, Entity, Generated, PrimaryGeneratedColumn } from 'typeorm';
import { Exclude } from 'class-transformer';

@Entity({
  name: 'Common_ApplicationEvent',
})
export class ApplicationEventEntity {
  @Exclude()
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({
    unique: true,
  })
  @Generated('uuid')
  eventIdentifier?: string;

  @Column()
  subject: string;

  @Column({
    nullable: true,
  })
  payload?: string;
}
