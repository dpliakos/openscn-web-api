/**
 *
 * @interface IResponse
 * @description The response format for the API
 *
 * @property {<T>} data The response payload should the request have one
 * @property {Error} error The error if any produced
 *
 */
export interface IResponse<T> {
  data?: T;
  error?: Error;
  total?: number;
  perPage?: number;
  page?: number;
}
