export interface IPaginationQuery {
  take?: number;
  skip?: number;

  page?: number;

  perPage?: number;
}
