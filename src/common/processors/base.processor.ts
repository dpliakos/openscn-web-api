import { OnWorkerEvent, WorkerHost } from '@nestjs/bullmq';
import { Logger } from '@nestjs/common';

export abstract class BaseProcessor extends WorkerHost {
  constructor(protected readonly _logger: Logger) {
    super();
  }

  @OnWorkerEvent('ready')
  onReady() {
    this._logger.verbose(`Ready`);
  }

  @OnWorkerEvent('failed')
  onFailed({ id }) {
    this._logger.verbose(`Job failed: ${id}`);
  }

  @OnWorkerEvent('completed')
  onCompleted(params) {
    this._logger.verbose(`Job completed: ${params.id}`);
  }

  @OnWorkerEvent('error')
  onError(err) {
    this._logger.error(err);
  }
}
