import {
  ApplicationEventSubject,
  IApplicationEvent,
  IApplicationEventPayload,
} from '../interfaces';

/**
 *
 * @interface SensorMeasurementCreatedEventPayload
 * @description The definition of a SensorMeasurementCreatedEvent payload
 *
 * @property {string} sensor The sensor identifier
 * @property {string} owner The sensor measurement owner identifier
 * @property {string} measurment The SensorMeasurement identifier
 * @property {boolean} isActive The isActive field
 * @property {boolean} isProduction The isProduction field
 *
 */
interface SensorMeasurementCreatedEventPayload
  extends IApplicationEventPayload {
  sensor?: string;
  actuator?: string;
  additionalType: string;
  owner: string;
  measurement: string;
  isActive: boolean;
  isProduction: boolean;
  version: number;
}

/**
 * @interface SensorMeasurementCreatedEvent
 * @description Declares that a sensor measurement was created
 *
 * @property {string} subject the event name
 * @property {SensorMeasurementCreatedEventPayload} payload The event data
 *
 */
export interface SensorMeasurementCreatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.SensorMeasurementCreated;
  payload: SensorMeasurementCreatedEventPayload;
}
