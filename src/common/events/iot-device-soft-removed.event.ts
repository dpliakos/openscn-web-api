import { ApplicationEventSubject, IApplicationEvent } from '../interfaces';

export interface IIoTDeviceSoftRemovedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.IoTDeviceSoftRemoved;
  payload: {
    identifier: string;
  };
}
