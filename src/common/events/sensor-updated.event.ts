import { ApplicationEventSubject, IApplicationEvent } from '../interfaces';

export interface SensorUpdatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.SensorUpdated;
  payload: {
    identifier: string;
    parentDevice?: string;
    // valueType?: 'int' | 'float' | 'boolean' | 'string';
    activeMeasurement?: string;
    testingMode?: boolean;
  };
}
