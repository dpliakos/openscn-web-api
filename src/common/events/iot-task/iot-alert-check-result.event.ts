import { ApplicationEventSubject, IApplicationEvent } from '../../interfaces';

export interface IoTTaskAlertCheckResultReceivedEvent
  extends IApplicationEvent {
  subject: ApplicationEventSubject.IoTTaskAlertCheckResultReceived;
  payload: {
    status: 'normal' | 'warning' | 'critical' | 'error';
    prevStatus?: 'normal' | 'warning' | 'critical' | 'error';
    jobId: string;
    task: string;
    workerVersion?: string;
    queueName?: string;
    error?: string;
    value: number;
  };
}
