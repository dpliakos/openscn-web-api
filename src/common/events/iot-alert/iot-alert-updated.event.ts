import { ApplicationEventSubject, IApplicationEvent } from '../../interfaces';

export interface IIoTAlertUpdatedEvent extends IApplicationEvent {
  subject: ApplicationEventSubject.IoTAlertUpdated;
  payload: {
    status: 'error' | 'normal' | 'warning' | 'critical';
    prevStatus?: 'error' | 'normal' | 'warning' | 'critical';
    alertConfiguration: string;
    acknowledged: boolean;
    alert: string;
  };
}
