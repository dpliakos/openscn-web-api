export interface RabbitmqBinding {
  source: string;
  vhost: string;
  destination: string;
  destination_type: string;
  routing_key: string;
  arguments: unknown;
  properties_key: string;
}
