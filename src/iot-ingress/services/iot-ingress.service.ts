import {
  ConflictException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InfluxdbService } from '../../time-series/services/influxdb.service';
import { IotDeviceIngresValueDTO } from '../dto/iot-device-ingress.dto';
import { IotDeviceService } from './iot-device/iot-device.service';
import { SensorEntity } from '../entities/sensor.entity';
import {
  EncapsulatedError,
  EncapsulatedErrorService,
  IoTValueType,
} from '../../common';
import { SensorMeasurementService } from './sensor-measurement/sensor-measurement.service';
import { SensorMeasurementEntity } from '../entities/sensor-measurement.entity';
import { SensorService } from './sensor/sensor.service';
import { IoTActuatorEntity } from '../entities';
import { IoTActuatorService } from './iot-actuator/iot-actuator.service';

@Injectable()
export class IotIngressService {
  private readonly _logger = new Logger(IotIngressService.name);

  constructor(
    private readonly _iotDeviceService: IotDeviceService,
    private readonly _influxDbService: InfluxdbService,
    private readonly _sensorMeasurementService: SensorMeasurementService,
    private readonly _sensorService: SensorService,
    private readonly _encapsulatedErrorService: EncapsulatedErrorService,
    private readonly _actuatorService: IoTActuatorService,
  ) {}

  async getActiveMeasurement(target: SensorEntity | IoTActuatorEntity) {
    return this._sensorMeasurementService.findOne({
      where: [
        {
          sensor: {
            identifier: target.identifier,
          },
          isActive: true,
        },
        {
          actuator: {
            identifier: target.identifier,
          },
          isActive: true,
        },
      ],
      relations: ['sensor', 'actuator'],
    });
  }

  async storeForDevice(dto: IotDeviceIngresValueDTO, timestamp?: Date) {
    let sensor: SensorEntity | undefined;
    let actuator: IoTActuatorEntity | undefined;

    try {
      const queries = [
        this._sensorService.findSensor({
          where: {
            identifier: dto.sensor,
          },
          relations: ['measurements'],
        }),
        this._actuatorService.findActuator({
          where: {
            identifier: dto.sensor,
          },
          relations: ['measurements', 'parentDevice'],
        }),
      ];

      [sensor, actuator] = (await Promise.all(queries)) as [
        SensorEntity,
        IoTActuatorEntity,
      ];
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    if (!sensor && !actuator) {
      const error = new NotFoundException(
        `Target ${dto.sensor} could not be found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    }

    if (
      sensor?.parentDevice !== dto.deviceIdentifier &&
      actuator?.parentDevice?.identifier !== dto.deviceIdentifier
    ) {
      const error = new ConflictException(
        `Target ${dto.sensor} does not belong to device ${dto.deviceIdentifier}`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(error, error),
      );
    }

    const targetType = sensor ? 'sensor' : 'actuator';
    const target = sensor ?? actuator;

    await this.store(dto, target, targetType, timestamp);
  }

  async store(
    dto: IotDeviceIngresValueDTO,
    target: SensorEntity | IoTActuatorEntity,
    targetType: 'sensor' | 'actuator',
    timestamp?: Date,
  ) {
    const activeMeasurement = await this.getActiveMeasurement(target);
    if (!activeMeasurement) {
      throw new Error(
        `${targetType} ${target.identifier} does not have active Measurement`,
      );
    }

    const measurement = {
      measurement: activeMeasurement?.identifier,
      targetId: target.identifier,
      type: targetType,
      fields: [
        {
          value: dto.value,
        },
        {
          device: dto.deviceIdentifier,
        },
        {
          valueType: target.valueType,
        },
      ],
      tags: [
        {
          testing: activeMeasurement.isProduction.toString(),
        },
      ],
      timestamp: timestamp.getTime() ?? Date.now(),
    };

    await this._influxDbService.store(measurement, this.getValueType(target));

    return 'ok';
  }

  async storeBulk(
    data: string[],
    dates: Date[],
    sensor: SensorEntity,
    extraTags: Record<string, string>,
  ) {
    const activeMeasurement = await this.getActiveMeasurement(sensor);

    if (!activeMeasurement) {
      throw new Error(
        `Sensor ${sensor.identifier} does not have active SensorMeasurement`,
      );
    }

    const formattedCollection = [];

    for (let i = 0; i < data.length; i++) {
      const formattedPoint = {
        measurement: activeMeasurement?.identifier,
        targetId: sensor.identifier,
        type: 'sensor',
        fields: [
          {
            value: data[i],
          },
          {
            device: sensor.parentDevice,
          },
          {
            valueType: sensor.valueType,
          },
        ],
        timestamp: dates[i],
        tags: [
          {
            testing: activeMeasurement.isProduction.toString(),
            ...extraTags,
          },
        ],
      };
      formattedCollection.push(formattedPoint);
    }

    await this._influxDbService.storeBulk(
      formattedCollection,
      sensor.valueType as IoTValueType,
    );
  }

  async removeBulk() {
    await this._influxDbService.removeBulk();
  }

  async getLastValue(measurement: SensorMeasurementEntity) {
    return this._influxDbService.getLast({
      measurement: measurement.identifier,
    });
  }

  getValueType(sensor: SensorEntity | IoTActuatorEntity): IoTValueType {
    switch (sensor.valueType) {
      case 'boolean':
        return IoTValueType.BOOLEAN;
      case 'string':
        return IoTValueType.STRING;
      case 'int':
        return IoTValueType.INT;
      case 'float':
        return IoTValueType.FLOAT;
      default:
        return IoTValueType.FLOAT;
    }
  }
}
