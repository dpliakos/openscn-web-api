import { InjectRepository } from '@nestjs/typeorm';
import { IoTDeviceEntity } from '../../entities/iot-device.entity';
import { Repository } from 'typeorm';
import { IoTDeviceTokenEntity } from '../../entities/iot-device-token.entity';
import {
  ApplicationEventSubject,
  EntityService,
  IIoTDeviceCreatedEvent,
  IIoTDeviceSoftRemovedEvent,
  IIoTDeviceTokenCreatedEvent,
  IIoTDeviceTokenRemovedEvent,
  IIoTDeviceTokenUpdatedEvent,
} from '../../../common';
import { Injectable, Logger } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { ApplicationEventsService } from '../../../common';

@Injectable()
export class IotDeviceService extends EntityService<IoTDeviceEntity> {
  private readonly _logger = new Logger(`IoTIngress/${IotDeviceService.name}`);

  constructor(
    @InjectRepository(IoTDeviceEntity)
    private readonly _iotDeviceEntityRepository: Repository<IoTDeviceEntity>,
    @InjectRepository(IoTDeviceTokenEntity)
    private readonly _iotDeviceTokenEntityRepository: Repository<IoTDeviceTokenEntity>,
  ) {
    super(IoTDeviceEntity.name);
    this.repository = _iotDeviceEntityRepository;
  }

  async findToken(identifier: string, token: string) {
    // TODO: check for token validity
    const tokenEntity = await this._iotDeviceTokenEntityRepository.findOne({
      where: {
        device: identifier,
        token: token,
      },
    });

    return tokenEntity;
  }

  @OnEvent(ApplicationEventSubject.IoTDeviceCreated)
  async onDeviceCreated(event: IIoTDeviceCreatedEvent['payload']) {
    this._logger.verbose(
      ApplicationEventsService.formatReceiverLog(
        ApplicationEventSubject.IoTDeviceCreated,
        event,
      ),
    );
    await this._iotDeviceEntityRepository.save({
      identifier: event.identifier,
    });
  }

  @OnEvent(ApplicationEventSubject.IoTDeviceSoftRemoved)
  async onDeviceSoftRemoved(event: IIoTDeviceSoftRemovedEvent['payload']) {
    this._logger.verbose(
      ApplicationEventsService.formatReceiverLog(
        ApplicationEventSubject.IoTDeviceSoftRemoved,
        event,
      ),
    );
    try {
      await this._iotDeviceEntityRepository.softDelete({
        identifier: event.identifier,
      });
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }
  }

  // iot device token events
  @OnEvent(ApplicationEventSubject.IoTDeviceTokenCreated)
  async onDeviceTokenCreated(event: IIoTDeviceTokenCreatedEvent['payload']) {
    this._logger.verbose(
      ApplicationEventsService.formatReceiverLog(
        ApplicationEventSubject.IoTDeviceTokenCreated,
        event,
      ),
    );
    await this._iotDeviceTokenEntityRepository.save({
      identifier: event.identifier,
      token: event.token,
      isValid: event.isValid,
      device: event.device,
    });
  }

  @OnEvent(ApplicationEventSubject.IoTDeviceTokenRemoved)
  async onDeviceTokenRemoved(event: IIoTDeviceTokenRemovedEvent['payload']) {
    this._logger.verbose(
      ApplicationEventsService.formatReceiverLog(
        ApplicationEventSubject.IoTDeviceTokenRemoved,
        event,
      ),
    );

    let token;

    try {
      token = await this._iotDeviceTokenEntityRepository.findOne({
        where: {
          identifier: event.identifier,
        },
      });
    } catch (err) {
      this._logger.error(err);
      return;
    }

    if (!token) {
      this._logger.error(
        `Received event to delete ${event.identifier}, but IoTDeviceToken with identifier ${event.identifier} does not exist`,
      );
    }

    try {
      await this._iotDeviceTokenEntityRepository.softDelete(token.id);
    } catch (err) {
      this._logger.error(err.stack ?? err);
    }
  }

  @OnEvent(ApplicationEventSubject.IoTDeviceTokenUpdated)
  async updateToken(event: IIoTDeviceTokenUpdatedEvent['payload']) {
    this._logger.verbose(
      ApplicationEventsService.formatReceiverLog(
        ApplicationEventSubject.IoTDeviceTokenUpdated,
        event,
      ),
    );
    const token = await this._iotDeviceTokenEntityRepository.findOne({
      where: {
        identifier: event.identifier,
      },
    });

    if (!token) {
      this._logger.error(
        `Receive ${ApplicationEventSubject.IoTDeviceTokenUpdated} for identifier ${event.identifier}. But record was not found`,
      );
    } else {
      const result = await this._iotDeviceTokenEntityRepository.update(
        {
          identifier: event.identifier,
        },
        {
          isValid: event.isValid,
        },
      );

      if (result.affected === 0) {
        this._logger.error(
          `Error handling ${ApplicationEventSubject.IoTDeviceTokenUpdated}`,
        );
      }
    }
  }
}
