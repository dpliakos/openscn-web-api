import { ApiProperty } from '@nestjs/swagger';
import {
  IsDate,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsUUID,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { IoTValueType } from '../../common';

export class IotDeviceIngresValueDTO {
  @ApiProperty()
  @IsNotEmpty()
  accessToken: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  deviceIdentifier: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  sensor: string;

  @ApiProperty()
  @IsNotEmpty()
  value: string;
}

export class IoTActuatorGetValue {
  @ApiProperty()
  @IsNotEmpty()
  accessToken: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  deviceIdentifier: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsUUID()
  actuator: string;
}

export class AddDemoDataDTO {
  @ApiProperty()
  @IsUUID()
  sensor: string;

  @ApiProperty()
  @IsEnum(IoTValueType)
  valueType: IoTValueType;

  @ApiProperty()
  @IsDate()
  @Transform((d) => new Date(d.value))
  @IsOptional()
  anchorDate?: Date;

  @ApiProperty()
  @IsNumber()
  @Transform((d) => Number(d.value))
  @IsOptional()
  timeStep?: number;
}

export class RemoveDemoDataDTO {
  @ApiProperty()
  @IsUUID()
  @IsOptional()
  sensor?: string;

  @ApiProperty()
  @IsUUID()
  @IsOptional()
  measurement?: string;
}
