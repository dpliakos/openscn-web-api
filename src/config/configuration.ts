import { readFileSync } from 'fs';
import * as yaml from 'js-yaml';
import { join } from 'path';
import { IConfiguration } from './config.inteface';
import * as process from 'process';
import { Logger } from '@nestjs/common';

const logger = new Logger('Configuration');

let prefix;
if (process.env.APP_ENV === 'docker') {
  prefix = 'docker';
} else if (process.env.APP_ENV === 'development') {
  prefix = 'development';
} else if (process.env.APP_ENV === 'staging') {
  prefix = 'staging';
}

const YAML_CONFIG_FILENAME = prefix ? `config.${prefix}.yaml` : 'config.yaml';

const path = join(__dirname, YAML_CONFIG_FILENAME);

logger.log(`Configuration path: ${path}`);

export default (): IConfiguration => {
  const fileConfig = yaml.load(
    readFileSync(path, 'utf8'),
  ) as Partial<IConfiguration>;

  return {
    ...fileConfig,
    app: {
      clientURL: fileConfig.app.clientURL,
      environment:
        process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'development'
          ? 'dev'
          : 'prod',
    },
  } as IConfiguration;
};
