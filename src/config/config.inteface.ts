import { DatabaseType } from 'typeorm';

/**
 *
 * @interface IDatabaseConfiguration
 * @description The list of fields for the database driver options
 *
 * @property {string} type The driver to use for the database
 * @property {string} host The server host of the database
 * @property {number} port The database port number
 * @property {string} username The database user's username
 * @property {string} password The database user's password
 * @property {string} database The database to select
 *
 */
export interface IDatabaseConfiguration {
  type: DatabaseType;
  host: string;
  port: number;
  username: string;
  password: string;
  database: string;
}

/**
 *
 * @interface IAuthConfig
 * @description Configuration options for the auth module
 *
 * @property { string } jwtSecret The secret to be used during the jwt signing
 * @property { string } jwtExpiration The expiration time for the jwt
 *
 */
export interface IAuthConfig {
  jwtSecret: string;
  jwtExpiration: string;
}

export interface IInfluxDbConfig {
  url: string;
  org: string;
  bucket: string;
  token: string;
}

export interface RedisConfig {
  url: string;
  port: number;
  username: string;
  password: string;
}

/**
 *
 * @interface ApplicationConfig
 * @description configuration that relates to the application
 *
 * @property {'dev' | 'prod'} environment The currently working environment
 * @property {string} clientUrl what is the expected url for the user facing portal
 *
 */
export interface ApplicationConfig {
  environment: 'dev' | 'prod';
  clientURL: string;
}

/**
 *
 * @interface EmailServer
 *
 * @property {string} host The email server hostname
 * @property {number} port The email server port
 * @property {boolean} ignoreTLS whether to ignore unencrypted content
 * @property {boolean} secure whether to use secure connection
 *
 */
export interface EmailServer {
  defaultSender: string;
  host: string;
  port: string;
  ignoreTLS: boolean;
  secure: boolean;
}

/**
 *
 * @interface RabbitMQ
 * @description Contains the rabbit mq configuration
 *
 * @property {string} url The url of the target server
 * @property {number} httpPort The port of that the server listens
 * @property {number} mqttPort The port of that the server listens
 * @property {string} username The username of the admin user
 * @property {string} password The password of the admin user
 * @property {string} vhost The virtual host alias
 * @property {string} mqttQueue The queue name to bind to the mqtt topic
 *
 */
export interface RabbitMQ {
  host: string;
  httpPort: number;
  mqttPort: number;
  username: string;
  password: string;
  vhost: string;
  mqttQueue?: string;
}

/**
 *
 * @interface IConfiguration
 * @description The list of available configuration items
 *
 * @property {IDatabaseConfiguration} database The database driver configuration
 * @property {IAuthConfig} auth The auth module configuration
 * @property {RabbitMQ} rabbitMQ The rabbit MQ configuration
 *
 */
export declare interface IConfiguration {
  database: IDatabaseConfiguration;
  auth: IAuthConfig;
  influx: IInfluxDbConfig;
  redis: RedisConfig;
  app: ApplicationConfig;
  email: EmailServer;
  rabbitMQ: RabbitMQ;
}
