import { Injectable, Logger } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import {
  ApplicationEventSubject,
  EventHandlerService,
  IUserCreatedEvent,
} from '../../../common';
import { SensorLabelService } from './sensor-label.service';
import { defaultLabelAdditionalName } from '../../iot-domain.constants';
import { UsersService } from '../../../users/services/users.service';
import { User } from '../../../users/entities/user.entity';

@Injectable()
export class SensorLabelEventHandlerService extends EventHandlerService {
  constructor(
    private readonly _sensorLabelService: SensorLabelService,
    private readonly _usersService: UsersService,
  ) {
    super(new Logger(SensorLabelEventHandlerService.name));
  }

  @OnEvent(ApplicationEventSubject.UserCreated)
  async onUserCreated(event: IUserCreatedEvent['payload']) {
    let user: User;

    try {
      user = await this._usersService.findOne({
        where: {
          id: event.id,
          email: event.email,
        },
      });
    } catch (err) {
      this._logger.error(err?.stack ?? err);
      return;
    }

    if (!user) {
      this._logger.error(
        `User with identifier ${event.identifier} was not found`,
      );
      return;
    }

    this.logEvent(ApplicationEventSubject.UserCreated, event);
    await this._sensorLabelService.save({
      additionalName: defaultLabelAdditionalName,
      name: 'Default',
      owner: user,
    });
  }
}
