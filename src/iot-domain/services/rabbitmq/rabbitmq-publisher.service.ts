import { Injectable, Logger } from '@nestjs/common';
import { IConfiguration } from '../../../config/config.inteface';
import { ConfigService } from '@nestjs/config';
import { IotDeviceEntity } from '../../entities';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const amqplib = require('amqplib');

@Injectable()
export class RabbitmqPublisherService {
  private channel;
  private connection;
  private readonly _logger = new Logger(
    `IoTDomain/${RabbitmqPublisherService.name}`,
  );

  constructor(private readonly _configService: ConfigService) {
    this.connect().catch((err) => this._logger.error(err));
  }

  async connect() {
    const config = this._configService.get(
      'rabbitmq',
    ) as IConfiguration['rabbitMQ'];
    this.connection = await amqplib.connect(
      `amqp://${config.username}:${config.password}@${config.host}:${config.mqttPort}`,
    );

    this.channel = await this.connection.createChannel();
  }

  async publishValue(
    device: IotDeviceEntity,
    target: string,
    value,
    format: 'json' | 'raw' = 'json',
  ) {
    // Verify if pubchannel is started
    if (!this.channel) {
      this._logger.error(
        "[AMQP] Can't publish message. Publisher is not initialized. You need to initialize them with StartPublisher function",
      );
      return;
    }
    // convert string message in buffer
    const payload =
      format === 'json'
        ? JSON.stringify({
            value: value,
          })
        : String(value);
    const message = Buffer.from(payload, 'utf-8');
    try {
      // Publish message to exchange
      // options is not required
      const routingKey = `.device.${device.identifier}.${target}`;
      await this.channel.publish(
        'amq.topic',
        routingKey,
        message,
        {},
        (err) => {
          if (err) {
            this._logger.error(err);
            this.channel.connection.close();
            return;
          }
          this._logger.debug(`Message published`);
        },
      );
      this._logger.debug(`Published message to ${routingKey}`);
    } catch (e) {
      this._logger.error(e);
    }
  }
}
