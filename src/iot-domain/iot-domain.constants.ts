export const MAX_DESCRIPTION_LENGTH = 300;

/**
 *
 * Every user must have at least on label
 *
 * This is the additionalName the every user's first label have
 *
 */
export const defaultLabelAdditionalName = 'defaultLabel';
