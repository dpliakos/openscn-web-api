import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
  MaxLength,
} from 'class-validator';
import { ResourceSearchQueryDTO } from '../../common';
import { MAX_DESCRIPTION_LENGTH } from '../iot-domain.constants';

export class SensorMeasurement {
  name: string;
}

export class CreateSensorMeasurementDTO {
  @ApiProperty()
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsOptional()
  @IsUUID()
  sensor?: string;

  @ApiProperty()
  @IsOptional()
  @IsUUID()
  actuator?: string;

  @ApiProperty()
  @IsOptional()
  @MaxLength(MAX_DESCRIPTION_LENGTH)
  description?: string;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  isActive?: boolean;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  isProduction?: boolean;
}

export class UpdateSensorMeasurementDTO {
  @ApiProperty()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  @MaxLength(MAX_DESCRIPTION_LENGTH)
  description?: string;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  isActive?: boolean;
}

export class SensorMeasurementQueryDTO extends ResourceSearchQueryDTO {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsUUID()
  identifier?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsUUID()
  sensor?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsUUID()
  actuator?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  isActive?: string;

  @ApiProperty({
    required: false,
  })
  @IsOptional()
  isProduction?: string;
}
