import {
  Controller,
  Get,
  UseGuards,
  Request,
  Post,
  Body,
  Delete,
  Param,
  Logger,
  ConflictException,
  InternalServerErrorException,
  NotFoundException,
  UseInterceptors,
  ClassSerializerInterceptor,
  Query,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../../auth';
import { SensorService } from '../../services/sensor/sensor.service';
import {
  EncapsulatedError,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  IResponse,
  IsDevModeGuard,
  PaginationQueryDto,
  QueryService,
} from '../../../common';
import { FindOptionsWhere, IsNull } from 'typeorm';
import { IotDeviceService } from '../../services/iot-device/iot-device.service';
import { SensorEntity } from '../../entities/sensor.entity';
import { SensorLabelService } from '../../services/sensor-label/sensor-label.service';
import {
  IoTActuatorTriggerActionCreateDTO,
  IoTActuatorTriggerActionQueryDTO,
} from 'src/iot-domain/dto/iot-actuator-trigger-action.dto';
import { IotActuatorTriggerActionEntity } from 'src/iot-domain/entities/iot-actuator-trigger-action.entity';
import { IotActuatorTriggerActionService } from 'src/iot-domain/services/iot-actuator-trigger/iot-actuator-trigger.service';
import { IotActuatorTriggerConfigurationEntity } from 'src/iot-domain/entities/iot-actuator-trigger-configuration.entity';
import { IotActuatorTriggerConfigurationService } from 'src/iot-domain/services/iot-actuator-trigger-configuration/iot-actuator-trigger-configuration.service';
import { IConfiguration } from '../../../config/config.inteface';
import { ConfigService } from '@nestjs/config';

@Controller('iot-actuator-trigger')
@ApiTags('iot-actuator-trigger')
@ApiBearerAuth()
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class IotActuatorTriggerActionController {
  private readonly _logger = new Logger(
    `IoTDomain/${IotActuatorTriggerActionController.name}`,
  );

  constructor(
    private readonly _sensorService: SensorService,
    private readonly _iotActuatorTriggerActionService: IotActuatorTriggerActionService,
    private readonly _iotActuatorTriggerConfigurationService: IotActuatorTriggerConfigurationService,
    private readonly _iotDeviceService: IotDeviceService,
    private readonly _encapsulatedErrorService: EncapsulatedErrorService,
    private readonly _queryService: QueryService,
    private readonly _sensorLabelService: SensorLabelService,
    private readonly _configService: ConfigService,
  ) {
    const env: IConfiguration['app']['environment'] =
      this._configService.get('app.environment');
    if (env === 'dev') {
      setImmediate(() => {
        this._logger.warn(
          `Running in dev mode. ${IotActuatorTriggerActionController.name} allows debugging endpoints`,
        );
      });
    }
  }

  @Get('/')
  async getUserIoTActuatorTriggerActions(
    @Request() req,
    @Query() paginationQuery: PaginationQueryDto,
    @Query() filtersQuery: IoTActuatorTriggerActionQueryDTO,
  ): Promise<IResponse<IotActuatorTriggerActionEntity[]>> {
    const { take, skip, page, perPage } =
      this._queryService.resolvePagination(paginationQuery);

    const { searchFactors } = this._queryService.resolveSearchQuery(
      filtersQuery,
      ['name', 'description'],
    );

    let actuatorTriggerActions: IotActuatorTriggerActionEntity[];
    let actuatorTriggerActionsCount: number;

    const whereClause: FindOptionsWhere<IotActuatorTriggerActionEntity> = {
      owner: {
        id: req.user.id,
      },
      identifier: filtersQuery?.identifier,
      configuration: {
        identifier: filtersQuery?.configuration,
      },
    };

    const whereClauseWithSearch =
      this._iotActuatorTriggerActionService.formatFindOptionsWithSearch(
        whereClause,
        searchFactors,
      );

    try {
      [actuatorTriggerActions, actuatorTriggerActionsCount] =
        await this._iotActuatorTriggerActionService.findAndCount({
          take,
          skip,
          where: whereClauseWithSearch,
          relations: ['configuration'],
          order: {
            createdAt: 'ASC',
          },
        });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    return {
      data: actuatorTriggerActions,
      total: actuatorTriggerActionsCount,
      page,
      perPage,
    };
  }

  @Get('/:identifier')
  async getIoTActuatorTriggerAction(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
  ): Promise<IResponse<IotActuatorTriggerActionEntity>> {
    const iotActuatorTriggerAction =
      await this._iotActuatorTriggerActionService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
          deletedDate: null,
        },
        order: {
          createdAt: 'DESC',
        },
        relations: ['configuration'],
      });
    if (!iotActuatorTriggerAction) {
      const notFoundError = new NotFoundException(
        `IotActuatorTriggerAction with identifier ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(notFoundError, notFoundError),
      );
    }

    return {
      data: iotActuatorTriggerAction,
    };
  }

  @Post('/')
  @UseGuards(IsDevModeGuard)
  async createIoTActuatorTriggerAction(
    @Request() req,
    @Body() dto: IoTActuatorTriggerActionCreateDTO,
  ): Promise<IResponse<SensorEntity>> {
    let triggerConfiguration: IotActuatorTriggerConfigurationEntity;

    try {
      triggerConfiguration =
        await this._iotActuatorTriggerConfigurationService.findOne({
          where: {
            owner: {
              id: req.user.id,
            },
            identifier: dto.configuration,
            deletedDate: IsNull(),
          },
        });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(this._logger, err);
    }

    if (!triggerConfiguration) {
      const badRequestError = new ConflictException(
        `IotActuatorTriggerConfiguration with identifier ${dto.configuration} does not exist`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(badRequestError, badRequestError),
      );
    }

    let actuatorTriggerAction;

    try {
      actuatorTriggerAction = await this._iotActuatorTriggerActionService.save({
        owner: req.user,
        configuration: triggerConfiguration,
        previousState: dto.previousState,
        targetValue: dto.targetValue,
      });
    } catch (err) {
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        new EncapsulatedError(err, new InternalServerErrorException()),
      );
    }

    return {
      data: actuatorTriggerAction,
    };
  }

  @Delete('/:identifier')
  async removeIotActuatorTriggerAction(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
  ): Promise<IResponse<IotActuatorTriggerActionEntity>> {
    const actuatorTriggerAction =
      await this._iotActuatorTriggerActionService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
          deletedDate: null,
        },
      });

    if (!actuatorTriggerAction) {
      const notFoundError = new NotFoundException(
        `IoTActuatorTriggerAction with identifier ${params.identifier} was not found`,
      );
      throw this._encapsulatedErrorService.formatError(
        this._logger,
        notFoundError,
      );
    }
    ``;
    const deleteResult = await this._iotActuatorTriggerActionService.softRemove(
      actuatorTriggerAction.id,
    );
    if (deleteResult.affected <= 0) {
      const error = new InternalServerErrorException(
        `IoTActuatorTriggerAction with identifier ${params.identifier} could not be deleted`,
      );
      throw this._encapsulatedErrorService.formatError(this._logger, error);
    }

    const updatedActuatorTriggerAction =
      await this._iotActuatorTriggerActionService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
        withDeleted: true,
      });

    return {
      data: updatedActuatorTriggerAction,
    };
  }
}
