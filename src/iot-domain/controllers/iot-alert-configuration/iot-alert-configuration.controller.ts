import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Logger,
  NotFoundException,
  Param,
  Post,
  Put,
  Query,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  BaseController,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  IResponse,
  PaginationQueryDto,
  QueryService,
} from '../../../common';
import { IoTAlertConfigurationEntity } from '../../entities/iot-alert-configuration.entity';
import { IoTAlertConfigurationService } from '../../services/iot-alert-configuration/iot-alert-configuration.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../../auth';
import {
  IoTAlertConfigurationCreateDTO,
  IoTAlertConfigurationQueryDTO,
  IoTAlertConfigurationUpdateDTO,
} from '../../dto/iot-alert-configuration.dto';
import { SensorEntity } from '../../entities/sensor.entity';
import { SensorService } from '../../services/sensor/sensor.service';
import {
  IoTTaskConfigurationDetailsType,
  IoTTaskConfigurationType,
  IoTTaskEntity,
  IoTTaskService,
  IoTTaskType,
} from '../../../iot-tasks';
import { IoTTaskCheckService } from '../../../iot-tasks/services/iot-task-check/iot-task-check.service';
import { IoTTaskCheckThresholdService } from '../../../iot-tasks/services/iot-check-threshold/iot-task-check-threshold.service';

@Controller('iot-alert-configuration')
@ApiTags('iot-alert-configuration')
@ApiBearerAuth()
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@UseInterceptors(ClassSerializerInterceptor)
export class IoTAlertConfigurationController extends BaseController<IoTAlertConfigurationEntity> {
  constructor(
    protected readonly _iotAlertConfigurationService: IoTAlertConfigurationService,
    protected readonly _queryService: QueryService,
    protected readonly _encapsulatedErrorService: EncapsulatedErrorService,
    private readonly _sensorService: SensorService,
    private readonly _iotTaskService: IoTTaskService,
    private readonly _ioTTaskCheckService: IoTTaskCheckService,
    private readonly _iotTaskCheckThesholdService: IoTTaskCheckThresholdService,
  ) {
    super(
      _iotAlertConfigurationService,
      _queryService,
      _encapsulatedErrorService,
      new Logger(`IoTDomain/${IoTAlertConfigurationService.name}`),
    );
  }

  @Get()
  async customGet(
    @Request() req,
    @Query() pagination: PaginationQueryDto,
    @Query() query: IoTAlertConfigurationQueryDTO,
  ) {
    const alertConfigurations = await super.get(
      req,
      pagination,
      query,
      {
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: query?.identifier,
          sensor: {
            identifier: query?.sensor,
          },
          isActive: query?.isActive,
        },
        relations: ['owner', 'task', 'sensor'],
      },
      ['name', 'description'],
    );

    const requests = alertConfigurations.data.map((alert) => {
      return this._iotTaskService.resolveConfigurationData(alert.task);
    });

    const populatedTasks = await Promise.all(requests);

    const newData = alertConfigurations.data.map((alert, index) => ({
      ...alert,
      task: populatedTasks[index],
    }));

    return {
      ...alertConfigurations,
      data: newData,
    };
  }

  @Get('/:identifier')
  async getOne(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
  ): Promise<IResponse<IoTAlertConfigurationEntity>> {
    const result = await super.getOne(req, params, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
      order: {
        createdAt: 'DESC',
      },
      relations: ['owner', 'task', 'sensor'],
    });

    result.data.task = await this._iotTaskService.resolveConfigurationData(
      result?.data?.task,
    );
    return result;
  }

  @Delete('/:identifier')
  async remote(@Request() req, @Param() params: EntityIdentifierDTO) {
    return super.remove(req, params, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
      relations: ['owner'],
    });
  }

  @Post()
  async create(
    @Request() req,
    @Body() dto: IoTAlertConfigurationCreateDTO,
  ): Promise<IResponse<IoTAlertConfigurationEntity>> {
    let configuration: IoTAlertConfigurationEntity;
    let sensor: SensorEntity;
    let task: IoTTaskEntity;

    try {
      sensor = await this._sensorService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: dto.sensor,
        },
        relations: ['measurements', 'owner'],
      });
    } catch (err) {
      throw this.getThrowable(err);
    }

    if (!sensor) {
      const err = new NotFoundException(`Sensor ${dto.sensor} was not found`);
      throw this.getThrowable(err, err);
    }

    try {
      const taskDeps = await this._iotTaskService.saveDeps(
        {
          owner: req.user,
          additionalType: IoTTaskConfigurationType.AlertCheck,
        },
        {
          owner: req.user,
          criticalThreshold: dto.criticalThreshold,
          warningThreshold: dto.warningThreshold,
          condition: dto.condition,
        },
      );

      const taskData = await this._iotTaskService.create({
        owner: req.user,
        additionalType: IoTTaskType.AlertCheckThreshold,
        configuration: taskDeps[0].identifier,
        configurationDetails: taskDeps[1].identifier,
        configurationType: IoTTaskConfigurationType.AlertCheck,
        detailsType: IoTTaskConfigurationDetailsType.ThresholdCheck,
        measurement: sensor.activeMeasurement.identifier,
      });

      task = await this._iotTaskService.save(taskData);
      configuration = await this._iotAlertConfigurationService.create({
        owner: req.user,
        name: dto.name,
        isActive: true,
        task: task,
        sensor: sensor,
      });

      const alert = await this._iotAlertConfigurationService.save(
        configuration,
      );
      alert.task.configurationData = taskDeps[0];
      alert.task.configurationDetailsData = taskDeps[1];

      return {
        data: alert,
      };
    } catch (err) {
      throw this.getThrowable(err);
    }
  }

  @Put(':identifier')
  async update(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: IoTAlertConfigurationUpdateDTO,
  ) {
    let configuration: IoTAlertConfigurationEntity;
    let updatedConfiguration: IoTAlertConfigurationEntity;

    try {
      configuration = await this._iotAlertConfigurationService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
        relations: ['task', 'owner'],
      });
    } catch (err) {
      throw this.getThrowable(err);
    }

    if (!configuration) {
      const error = new NotFoundException(
        `IoTAlertConfiguration ${params.identifier} was not found`,
      );
      throw this.getThrowable(error, error);
    }

    if (dto.isActive === false && configuration.task?.enabled === true) {
      await this._iotTaskService.save({
        ...configuration.task,
        enabled: false,
      });
    } else if (dto.isActive && !configuration.isActive) {
      await this._iotTaskService.save({
        ...configuration.task,
        enabled: true,
      });
    }

    if (dto.condition || dto.warningThreshold || dto.criticalThreshold) {
      await this._iotTaskService.updateDetails(configuration.task, {
        condition: dto.condition,
        warningThreshold: dto.warningThreshold,
        criticalThreshold: dto.criticalThreshold,
      });
    }

    const updatableFields = this.filterUpdatableFields(dto, [
      'name',
      'description',
      'isActive',
    ]);
    try {
      const updated = await this._iotAlertConfigurationService.create({
        ...configuration,
        ...updatableFields,
      });

      await this._iotAlertConfigurationService.save(updated);
      updatedConfiguration = await this._iotAlertConfigurationService.findOne({
        where: {
          id: configuration.id,
        },
        relations: ['owner', 'task'],
      });
    } catch (err) {
      throw this.getThrowable(err);
    }

    return {
      data: updatedConfiguration,
    };
  }

  async remove(@Request() req, @Param() params: EntityIdentifierDTO) {
    return super.remove(req, params, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
    });
  }
}
