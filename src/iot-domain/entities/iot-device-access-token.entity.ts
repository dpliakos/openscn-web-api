import { Column, Entity, ManyToOne } from 'typeorm';
import { IotDeviceEntity } from './iot-device.entity';
import { OwnedThing } from '../../users/entities/owned-thing.entity';

/**
 *
 * @class IotDeviceAccessTokenEntity
 * @description Describes a token that is being used to authorize an IoT device
 *
 * @property {string} token The token that is being used
 * @property {IotDevice} device The parent device
 * @property {number} ttl The time to live for the token
 * @property {boolean} isValid Flags whether this token can be used
 *
 */
@Entity({
  name: 'IoTDomain_IoTDeviceAccessToken',
})
export class IotDeviceAccessTokenEntity extends OwnedThing {
  @Column({
    type: 'text',
  })
  token: string;

  @Column({
    type: 'bool',
    default: true,
  })
  isValid: boolean;

  @Column({
    type: 'int',
    default: 30,
  })
  ttl?: number;

  @ManyToOne(() => IotDeviceEntity, (device) => device.id)
  device: IotDeviceEntity;
}
