import { Injectable } from '@nestjs/common';
import { UserAccountLocal } from '../entities/user-account-local.entity';
import { EntityService } from '../../common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthTokenEntity } from '../entities/auth-token.entity';
import { UserAccount } from '../entities/user-account.entity';
import { PasswordService } from './password.service';
import { AuthTokenService } from './auth-token.service';

@Injectable()
export class UserAccountLocalService extends EntityService<UserAccountLocal> {
  constructor(
    @InjectRepository(UserAccountLocal)
    private readonly _userAccountLocalRepository: Repository<UserAccountLocal>,
    private readonly _passwordService: PasswordService,
    private readonly _resetTokenService: AuthTokenService,
  ) {
    super();
    this.repository = _userAccountLocalRepository;
  }

  async resetPasswordWithToken(options: {
    token: AuthTokenEntity;
    localAccount: UserAccountLocal;
    newPassword: string;
  }): Promise<void> {
    const newPassword = await this._passwordService.toHash(options.newPassword);

    await this._userAccountLocalRepository.manager.transaction(async () => {
      await this.save({
        ...options.localAccount,
        password: newPassword,
      });

      return await this._resetTokenService.save({
        ...options.token,
        usedAt: new Date(),
        active: false,
      });
    });
  }
}
