import { Injectable, Logger } from '@nestjs/common';
import {
  ApplicationEventSubject,
  EventHandlerService,
  IIoTAlertCreatedEvent,
} from '../../../common';
import { OnEvent } from '@nestjs/event-emitter';
import { NotificationService } from './notification.service';
import { IoTAlertService } from '../../../iot-domain/services/iot-alert/iot-alert.service';
import { IIoTAlertUpdatedEvent } from '../../../common/events/iot-alert/iot-alert-updated.event';
import { MeasurementUnitService } from '../../../iot-domain/services/measurement-unit/measurement-unit.service';
import { In } from 'typeorm';
import { MeasurementUnitEntity } from '../../../iot-domain/entities/measurement-unit.entity';

@Injectable()
export class NotificationHandlerService extends EventHandlerService {
  constructor(
    private readonly _notificationService: NotificationService,
    private readonly _iotAlertService: IoTAlertService,
    private readonly _measurementUnitService: MeasurementUnitService,
  ) {
    super(new Logger(`Client/${NotificationHandlerService.name}`));
  }

  @OnEvent(ApplicationEventSubject.IoTAlertCreated)
  async onIoTAlertCreated(data: IIoTAlertCreatedEvent['payload']) {
    this.logEvent(ApplicationEventSubject.IoTAlertCreated, data);

    const alert = await this._iotAlertService.findOne({
      where: {
        identifier: data.alert,
      },
      relations: [
        'owner',
        'alertConfiguration',
        'alertConfiguration.sensor',
        'alertConfiguration.sensor.measurementUnit',
      ],
    });

    let measurementUnit: MeasurementUnitEntity;

    try {
      measurementUnit = await this._measurementUnitService.findOne({
        where: {
          identifier:
            alert.alertConfiguration.sensor.measurementUnit.identifier,
          owner: {
            id: In([global.publicUserId, alert.owner.id]),
          },
        },
      });
    } catch (err) {
      this._logger.error(err?.stack ?? err);
    }

    const textBody = `${data.status?.toUpperCase()} from ${data.prevStatus?.toUpperCase()} status for ${
      alert.alertConfiguration.name
    } of sensor ${alert?.alertConfiguration?.sensor?.name} with value ${
      alert.value
    }${measurementUnit ? measurementUnit.symbol : ''}`;

    this._iotAlertService
      .findOne({
        where: {
          identifier: data.alert,
        },
        relations: ['owner'],
      })
      .then((alert) => {
        this._notificationService
          .save({
            owner: alert.owner,
            resourceName: 'IoTAlert',
            instanceIdentifier: alert.identifier,
            resourcePath: 'iot-alert',
            description: textBody,
          })
          .catch((err) => this._logger.error(err.stack ?? err));
      })
      .catch((err) => this._logger.error(err.stack ?? err));
  }

  @OnEvent(ApplicationEventSubject.IoTAlertUpdated)
  async onIoTAlertUpdated(data: IIoTAlertUpdatedEvent['payload']) {
    this.logEvent(ApplicationEventSubject.IoTAlertUpdated, data);
    this._notificationService
      .findAll({
        where: {
          instanceIdentifier: data.alert,
          resourceName: 'IoTAlert',
        },
      })
      .then((results) => {
        this._notificationService.markAsAcknowledged(results).catch((err) => {
          this._logger.error(err?.stack ?? err);
        });
      })
      .catch((err) => {
        this._logger.error(err?.stack ?? err);
      });
  }
}
