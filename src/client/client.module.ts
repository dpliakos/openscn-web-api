import { Module } from '@nestjs/common';
import { CommonModule } from '../common';
import { AuthModule } from '../auth/auth.module';
import { UsersModule } from '../users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NotificationEntity } from './entities/notification.entity';
import { NotificationService } from './services/notification/notification.service';
import { NotificationHandlerService } from './services/notification/notification.handler.service';
import { NotificationController } from './controllers/notification.controller';
import { IotDomainModule } from '../iot-domain/iot-domain.module';
import { ClientController } from './controllers/client.controller';

@Module({
  imports: [
    CommonModule,
    AuthModule,
    UsersModule,
    TypeOrmModule.forFeature([NotificationEntity]),
    IotDomainModule,
  ],
  providers: [NotificationService, NotificationHandlerService],
  controllers: [NotificationController, ClientController],
})
export class ClientModule {}
