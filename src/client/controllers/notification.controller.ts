import {
  BaseController,
  EncapsulatedErrorService,
  EntityIdentifierDTO,
  IResponse,
  PaginationQueryDto,
  QueryService,
} from '../../common';
import { NotificationEntity } from '../entities/notification.entity';
import { NotificationService } from '../services/notification/notification.service';
import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Logger,
  NotFoundException,
  Param,
  Put,
  Query,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  NotificationQueryDTO,
  NotificationUpdateDTO,
} from '../dto/notification.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { IsUserAuthGuard, ValidJwtAuthGuard } from '../../auth';

@Controller('client-notification')
@ApiTags('user-notification')
@UseGuards(ValidJwtAuthGuard, IsUserAuthGuard)
@ApiBearerAuth()
@UseInterceptors(ClassSerializerInterceptor)
export class NotificationController extends BaseController<NotificationEntity> {
  constructor(
    protected readonly _notificationService: NotificationService,
    protected readonly _queryService: QueryService,
    protected readonly _encapsulatedErrorService: EncapsulatedErrorService,
  ) {
    super(
      _notificationService,
      _queryService,
      _encapsulatedErrorService,
      new Logger(NotificationController.name),
    );
  }

  @Get()
  async get(
    @Request() req,
    @Query() pagination: PaginationQueryDto,
    @Query() query: NotificationQueryDTO,
  ) {
    return super.get(req, pagination, query, {
      where: {
        owner: {
          id: req.user.id,
        },
        acknowledged: query.acknowledged,
      },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  @Get(':identifier')
  async getOne(@Request() req, @Param() params: EntityIdentifierDTO) {
    return super.getOne(req, params, {
      where: {
        owner: {
          id: req.user.id,
        },
        identifier: params.identifier,
      },
      order: {
        createdAt: 'DESC',
      },
    });
  }

  @Put(':identifier')
  async update(
    @Request() req,
    @Param() params: EntityIdentifierDTO,
    @Body() dto: NotificationUpdateDTO,
  ): Promise<IResponse<NotificationEntity>> {
    let notification: NotificationEntity;
    let updatedNotification: NotificationEntity;

    try {
      notification = await this._notificationService.findOne({
        where: {
          owner: {
            id: req.user.id,
          },
          identifier: params.identifier,
        },
      });
    } catch (err) {
      throw this.getThrowable(err);
    }

    if (!notification) {
      const error = new NotFoundException(`${this._notificationService.name}`);
      throw this.getThrowable(error, error);
    }

    if (dto.acknowledged !== undefined) {
      updatedNotification = await this._notificationService.save({
        ...notification,
        acknowledged: dto.acknowledged,
      });

      return {
        data: updatedNotification,
      };
    } else {
      return {
        data: notification,
      };
    }
  }
}
