import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsNumber, IsOptional, IsUUID } from 'class-validator';

export class PublishDebuggingWorkerEventDTO {
  @ApiProperty()
  @IsUUID()
  measurement: string;

  @ApiProperty()
  @IsOptional()
  start?: string;

  @ApiProperty()
  @IsOptional()
  stop?: string;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  warningThreshold?: number;

  @ApiProperty()
  @IsNumber()
  criticalThreshold: number;

  @ApiProperty()
  @IsIn(['gt', 'lt'])
  operator: 'gt' | 'lt';

  @ApiProperty()
  @IsUUID()
  task: string;
}
