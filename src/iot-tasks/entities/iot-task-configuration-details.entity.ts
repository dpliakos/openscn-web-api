import { OwnedThing } from '../../users/entities/owned-thing.entity';

export abstract class IoTTaskConfigurationDetailsEntity extends OwnedThing {
  constructor(props) {
    super(props);
  }
}
