/* eslint-disable @typescript-eslint/no-var-requires */

const dotenv = require('dotenv');
dotenv.config();

const { DataSource } = require('typeorm');
const commonEntities = require('./dist/common/entities');
const userEntities = require('./dist/users/entities');
const authEntities = require('./dist/auth/entities');
const iotDomainEntities = require('./dist/iot-domain/entities');
const iotIngressEntities = require('./dist/iot-ingress/entities');
const iotQueryEntities = require('./dist/iot-query/entities');
const iotTaskEntities = require('./dist/iot-tasks/entities');
const clientEntities = require('./dist/client/entities');

const PostgresDataSource = new DataSource({
  type: "postgres",
  host: process.env.DATABASE_HOST,
  port: process.env.DATABASE_PORT ?? 5432,
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  migrations: ["./migrations/*.js"],
  entities: [
    ...Object.values(commonEntities),
    ...Object.values(userEntities),
    ...Object.values(authEntities),
    ...Object.values(iotTaskEntities),
    ...Object.values(clientEntities),
    ...Object.values(iotDomainEntities),
    ...Object.values(iotQueryEntities),
    ...Object.values(iotIngressEntities),
  ],
});

module.exports["default"] = PostgresDataSource;
